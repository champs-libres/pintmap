# coding: utf-8

from PyQt5.QtWidgets import QDialog, QDialogButtonBox, QApplication, QTextBrowser, QDesktopWidget, QVBoxLayout, \
    QPushButton
from PyQt5.QtCore import Qt, QSize, QUrl
from PyQt5.QtWebKitWidgets import QWebView
import sys
import codecs
import os


class InfoDialog(QDialog):

    def __init__(self):
        QDialog.__init__(self)

        # self.resize(550, 340)
        self.setFixedSize(550, 340)
        self.setWindowTitle("Help")

        fg = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        fg.moveCenter(cp)
        self.move(fg.topLeft())

        self.vertLayout = QVBoxLayout(self)
        self.vertLayout.setSpacing(5)
        # self.vertLayout.setContentsMargins(1,1,2,4)
        self.vertLayout.setAlignment(Qt.AlignTop)
        self.setLayout(self.vertLayout)

        # Load information file
        file_name = "info.html"
        info_path = os.path.normpath(os.path.join(os.path.dirname(__file__), "template"))
        file_path = os.path.join(info_path, file_name)
        text = codecs.open(file_path, "r", "utf-8").read()

        self.webView = QWebView(self)
        # self.webView.setMinimumSize(QSize(0, 260))
        self.webView.setUrl(QUrl("about:blank"))
        self.webView.setObjectName("webView")
        self.webView.setHtml(text)

        self.buttonBox = QDialogButtonBox(QDialogButtonBox.Ok)
        self.buttonBox.accepted.connect(self.close_dialog)

        # Add widgets to layout
        self.vertLayout.addWidget(self.webView)
        self.vertLayout.addWidget(self.buttonBox)

    def close_dialog(self):
        self.close()


if __name__ == '__main__':
    app = QApplication(sys.argv)

    dialog = InfoDialog()
    dialog.show()

    sys.exit(app.exec_())
