# coding: utf-8
"""
/***************************************************************************
 PintDialog
                                 A QGIS plugin
 Photo interpretation
                             -------------------
        begin                : 2014-07-02
        copyright            : (C) 2014 by GbxABT
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from qgis.PyQt.QtCore import Qt
from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtWidgets import QDockWidget, QWidget, QMessageBox
import os.path
from qgis.PyQt import uic

uiLoadPintDialog = uic.loadUiType(os.path.join(os.path.dirname(__file__), 'ui_pint.ui'))[0]

# create the dialog for zoom to point
class PintDialog(QDockWidget, uiLoadPintDialog):
    def __init__(self, pint, parent=None):
        # Conversion de l'interface vers un QDockWidget
        QDockWidget.__init__(self)
        super(PintDialog, self).__init__(parent)
        self.pint = pint
        self.setWindowFlags(Qt.WindowSystemMenuHint | Qt.WindowTitleHint)
        # Set up the user interface from Designer.
        self.setupUi(self)
        self.setWindowIcon(QIcon(":/plugins/PintMap/icon.png"))
        self.prepare_display()

    def closeEvent(self, event):
        msg = "Are you sure you want to exit PintMap ?"
        reply = QMessageBox.question(self, 'Message', msg, QMessageBox.Yes, QMessageBox.No)
        if reply == QMessageBox.Yes:
            event.accept()

            self.pint.close_plugin()
        else:
            event.ignore()

    def prepare_display(self):
        # AlignTop
        self.verticalLayout_3.setAlignment(Qt.AlignTop)
