# PintMap plugin

## Table of contents
1. [Introduction](#introduction)
2. [Features description](#featuresdescription)
    1. [Layer](#layer)
    2. [Create Layer](#createlayer)
    3. [Load layer](#loadlayer)
    4. [Enter values & Load values](#entervalues&loadvalues)
    5. [Attribute column & add column](#attributecolumn&addcolumn)
3. [Values](#values)
    1. [Update](#update)
    2. [Flag](#flag)
    3. [Comment](#comment)
    4. [4 shortcuts buttons](#4shortcutsbuttons)
    5. [Def. value](#defvalue)
    6. [Current value](#currentvalue)
    7. [Usage info](#usageinfo)
4. [Search](#search)
5. [Other Features](#otherfeatures)


## Introduction <a name="introduction"></a>
Hello!
PintMap (standing for Photo Interpretation) is as you might have guessed a photo interpretation plugin. Its aim is to make photo interpretation faster and less tedious.

The plugin was first created by Michael Pichugin for Gembloux Agro-Bio Tech in 2014 and was updated in 2021 by Gaspard Estenne still for Gembloux Agro-Bio Tech with the help of Champs-Libres SCRL.

You will find PintMaps users interface below so you can visualize the button mentionned in this README.

![Pintmaps User Interface](/ui_image.PNG)


## Features description <a name="featuresdescription"></a>
I will now describe briefly the effects of the buttons of the user interface.
From top to bottom of the plugins window, we first have the layer box.
### Layer <a name="layer"></a>
#### Create Layer <a name="createlayer"></a>
Here you have two buttons, the first one is `Create layer`. If clicked, a dialog box will appear letting you choose a the boundaries of the grid layer you want to create (as a vector layer) and the distance between the points on the grid.
Once you have entered these parameters, you can test them by clicking on the `Try` button, creating a temporary grid layer with thoses parameters. 
If this grid layer suits you, you can save it by entering the name you want to give to it in the third text box and then by clicking on `Save`. You will then have to choose the saving directory.
#### Load Layer <a name="loadlayer"></a>
If the options of the Create Layer feature don't suit you or if you already have the layer you want to interpret, you may just load it by clicking on `Load Layer`. This will load the layer selected on the current project.
#### Enter values & Load values <a name="entervalues&loadvalues"></a>
Moving on to the Values. Here you also have two options which are similar to the two Layer options. You can either click on `Enter values` which make a dialog window appear and lets you enter up to 6 land use classes. If you want to enter more then 6 classes, you will have to create a .txt file just like this:

```
val,rank
FOREST,1
CROP,2
GRASS,3
ARTI,4
WETLANDS,5
OTHER,6
```

which you can then load using `Load values`. Note that with this option you can make as many classes as you want. The values of this example are the IPCC land use classes.

### 3. Values <a name="values"></a>
#### 3.1. Update <a name="update"></a>
The `Update` button allows you to update the value of the land use class for the selected point. The value of the Set value scrolling menu will then be written. 
`Update >>` does the same as `Update` except that it makes you pass to the next point too. This is the case for all the buttons with `>>`.

#### 3.2. Flag <a name="flag"></a>
The Flag check box allows you to set the value of the Flag column to 1.

#### 3.3. Comment <a name="comment"></a>
The `Comment` button write the comment written in the text box for the point interpreted in the comment column of the attribute table.

#### 3.4. 4 shortcuts buttons <a name="4shortcutsbuttons"></a>
These 4 buttons get the values of the 4 first classes writtent in the .txt file (or the 4 first you entered using `Enter values`). They write their value in the attribute table in the land use class column and pass to the next point.

#### 3.5. Def. value <a name="defvalue"></a>
This scrolling menu allows you to set the default value for the first scrolling menu. This way you can have a direct access to 5 land use classes.

#### 3.6. Current value <a name="currentvalue"></a>
The Current value text box displays the current value of land use registered in the attribute table for the point selected. The `Select all` button selects all the points that have the same value as the one in the Current value text box.
The two `<<` and `>>` buttons allow you to navigate back and forth in the points of the grid.

#### 3.7. Usage info <a name="usageinfo"></a>
The `Usage Info` button displays a windows giving basic informations and statistics on the interpretation in process like the number of points you already have interpreted or the total for each land use class.


### Search <a name="search"></a>
This section allows you to search and hopefully find certain values. You can use the scrolling menu if you just want to find one value or you can use the expression just under it and you will there be able to find all the points that correspond to your research and to navigate through them. 
You will have to use an expression for this just as the example that is shown on the user interface.

### Other features <a name="otherfeatures"></a>
When you load a layer in the plugin, a column called num_value is generated in the attribute table of the layer. In this column, numbers will be generated corresponding to the land use classes you work with. The point of this column is to be used for example  with OTB or similar toolboxes that work better with numeric values rather than with strings.




