# -*- coding: utf-8 -*-
"""
/***************************************************************************
 PintMap
                                 A QGIS plugin
 Photo interpretation
                              -------------------
        begin                : 2014-07-02
        copyright            : (C) 2014 by GbxABT
        email                : gaspard.estenne@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 MP 110517: Converted from int to string attribute, ui update and bugs fixed
"""
# Import the PyQt and QGIS libraries
from qgis.core import QgsVectorLayer, QgsMapLayer, QgsField, QgsProject, QgsWkbTypes, QgsFeatureRequest, QgsProcessingException, \
QgsMapLayerLegendUtils
from qgis.utils import iface
from qgis._gui import QgsMapToolEmitPoint, QgsMapCanvas
from qgis.PyQt.QtCore import Qt, QSettings, QTimer, QTranslator, qVersion, QCoreApplication, QVariant, QLocale
from qgis.PyQt.QtGui import QValidator, QIntValidator, QDoubleValidator, QIcon
from qgis.PyQt.QtWidgets import QAction, QSizePolicy, QComboBox, QCompleter, QMessageBox, QFileDialog

# Initialize Qt resources from file resources.py
from . import resources
# Import the code for the dialog
from .addcoldialog import AddColDialog
from .createlayerdialog import CreateLayerDialog
from .entervaluesdialog import EnterValuesDialog
from .pintdialog import PintDialog
from .infodialog import InfoDialog
from .usagedialog import UsageDialog
import processing
import csv
import os.path
import traceback
from pathlib import Path
import shutil
from datetime import datetime


class Pint:

    def __init__(self, iface):
        # Save reference to the QGIS interface
        self.iface = iface
        # A reference to our map canvas
        self.canvas = self.iface.mapCanvas()
        # Declaration attibute dialog
        self.dlg1 = None
        self.clickTool = QgsMapToolEmitPoint(self.canvas)
        # Initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)

        # Do stuff
        self.connected = False
        self.version = "2.0"
        self.layer = iface.activeLayer()
        self.f_name = None
        self.com_col = None
        # Selected field index
        self.field_idx = None
        self.fid_to_idx = None
        self.idx_to_fid = None
        self.feat_id_lst = None
        self.search_lst_pos = None
        self.flag_idx = None
        self.values = []
        self.ranks = []
        self.data_path = None

        # Initialize locale
        locale = QSettings().value("locale/userLocale")[0:2]
        localePath = os.path.join(self.plugin_dir, 'i18n', 'pint_{}.qm'.format(locale))

        if os.path.exists(localePath):
            self.translator = QTranslator()
            self.translator.load(localePath)

            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)

        # Create the dialog (after translation) and keep reference
        self.dlg1 = PintDialog(self)
        self.dlg2 = CreateLayerDialog()
        self.dlg3 = EnterValuesDialog()
        self.dlg4 = AddColDialog()


    def initGui(self):
        # Create action that will start plugin configuration
        self.action = QAction(
            QIcon(":/plugins/pintmap/icon.png"),
            "PintMap", self.iface.mainWindow())

        # Add toolbar button and menu item
        self.iface.addToolBarIcon(self.action)
        self.iface.addPluginToMenu("&PintMap", self.action)

        # Misc.
        self.dlg1.txtCurrentValue.setText("")

        # Set listeners
        self.action.triggered.connect(self.run)
        self.connect_listeners()

    def connect_listeners(self):
        self.dlg4.btnACok.clicked.connect(self.addColOk)
        self.dlg4.btnACCancel.clicked.connect(self.addColCancel)
        self.dlg3.btnEVOK.clicked.connect(self.enterValuesOk)
        self.dlg3.btnEVCancel.clicked.connect(self.enterValuesCancel)
        self.dlg2.btnCLTest.clicked.connect(self.createLayerTest)
        self.dlg2.btnCLCancel.clicked.connect(self.createLayerClose)
        self.dlg2.btnCLSave.clicked.connect(self.createLayerSave)
        self.dlg1.btnAddCol.clicked.connect(self.addCol)
        self.dlg1.btnEnterValues.clicked.connect(self.enterValues)
        self.dlg1.btnCreateLayer.clicked.connect(self.createLayer)
        self.dlg1.btnLoadPoints.clicked.connect(self.loadVectorLayer)
        self.dlg1.btnUpdate.clicked.connect(lambda: self.updateField(None))
        self.dlg1.btncomment.clicked.connect(self.commentValue)
        self.dlg1.btnUpdateNext.clicked.connect(self.updateFieldNext)
        self.dlg1.btnSearch.clicked.connect(self.searchValue)
        self.dlg1.btnNextId.clicked.connect(self.nextID)
        self.dlg1.btnPreviousId.clicked.connect(self.previousID)
        self.dlg1.btnSetSelectTool.clicked.connect(self.setSelectTool)
        self.dlg1.btnLoadList.clicked.connect(self.loadValuesDef)
        self.dlg1.btnRequestSearch.clicked.connect(self.searchExpression)
        self.dlg1.btnRequestNext.clicked.connect(self.requestNext)
        self.dlg1.btnRequestPrevious.clicked.connect(self.requestPrevious)
        self.dlg1.firstColCombo.currentIndexChanged.connect(self.fieldChanged)
        self.dlg1.btnInfoSearch.clicked.connect(self.showInfoSearch)
        self.dlg1.btnInfoUsage.clicked.connect(self.showInfoUsage)
        # Ranks btns
        self.dlg1.btnRank1.clicked.connect(lambda: self.rankUpdateNext(0))
        self.dlg1.btnRank2.clicked.connect(lambda: self.rankUpdateNext(1))
        self.dlg1.btnRank3.clicked.connect(lambda: self.rankUpdateNext(2))
        self.dlg1.btnRank4.clicked.connect(lambda: self.rankUpdateNext(3))
        QgsProject.instance().layersWillBeRemoved.connect(self.layersRemoved)
        # Flag
        self.connected = True

    def disconnect_listeners(self):
        self.dlg4.btnACok.clicked.disconnect(self.addColOk)
        self.dlg4.btnACCancel.clicked.disconnect(self.addColCancel)
        self.dlg3.btnEVOK.clicked.disconnect(self.enterValuesOk)
        self.dlg3.btnEVCancel.clicked.disconnect(self.enterValuesCancel)
        self.dlg2.btnCLTest.clicked.disconnect(self.createLayerTest)
        self.dlg2.btnCLCancel.clicked.disconnect(self.createLayerClose)
        self.dlg2.btnCLSave.clicked.disconnect(self.createLayerSave)
        self.dlg1.btnAddCol.clicked.disconnect(self.addCol)
        self.dlg1.btnEnterValues.clicked.disconnect(self.enterValues)
        self.dlg1.btnCreateLayer.clicked.disconnect(self.createLayer)
        self.dlg1.btnLoadPoints.clicked.disconnect(self.loadVectorLayer)
        # Disconnect all slots for lambda linked functions
        self.dlg1.btnUpdate.clicked.disconnect()
        self.dlg1.btnUpdateNext.clicked.disconnect(self.updateFieldNext)
        self.dlg1.btncomment.clicked.disconnect(self.commentValue)
        self.dlg1.btnSearch.clicked.disconnect(self.searchValue)
        self.dlg1.btnNextId.clicked.disconnect(self.nextID)
        self.dlg1.btnPreviousId.clicked.disconnect(self.previousID)
        self.dlg1.btnSetSelectTool.clicked.disconnect(self.setSelectTool)
        self.dlg1.btnLoadList.clicked.disconnect(self.loadValuesDef)
        self.dlg1.btnRequestSearch.clicked.disconnect(self.searchExpression)
        self.dlg1.btnRequestNext.clicked.disconnect(self.requestNext)
        self.dlg1.btnRequestPrevious.clicked.disconnect(self.requestPrevious)
        self.dlg1.firstColCombo.currentIndexChanged.disconnect(self.fieldChanged)
        self.dlg1.btnInfoSearch.clicked.disconnect(self.showInfoSearch)
        self.dlg1.btnInfoUsage.clicked.disconnect(self.showInfoUsage)
        # Ranks btns, disconnect all slots for lambda linked functions
        self.dlg1.btnRank1.clicked.disconnect()
        self.dlg1.btnRank2.clicked.disconnect()
        self.dlg1.btnRank3.clicked.disconnect()
        self.dlg1.btnRank4.clicked.disconnect()
        QgsProject.instance().layersWillBeRemoved.disconnect(self.layersRemoved)
        # Flag
        self.connected = False

    def unload(self):
        # Remove the plugin menu item and icon
        self.iface.removePluginMenu("&PintMap", self.action)
        self.iface.removeToolBarIcon(self.action)

    # Run method that performs all the real work
    def run(self):
        self.f_name = ""
        self.com_col = ""
        self.field_idx = -1
        self.fid_to_idx = {}
        self.idx_to_fid = {}
        self.feat_id_lst = []
        self.search_lst_pos = -1
        self.data_path = Path(self.resolve("data"))

        if not self.connected:
            self.connect_listeners()

        self.dlg1.lblRequestCounter.setText("")
        self.dlg1.txtCurrentValue.setText("")
        self.dlg1.txtSearchCurrentValue.setText("")
        self.dlg1.teeRequest.setText("")
        self.dlg1.lblOrder.setText("")

        # Add code values (string)
        self.values = []
        self.ranks = []
        self.dlg1.cbbValues.clear()
        self.dlg1.cbbFindValues.clear()
        self.dlg1.cbbDefaultValues.clear()

        # Just load default values
        self.loadValues(ask_path=False)

        # Deselect all
        # Self.layer.removeSelection()
        self.dlg1.txtSearchCurrentValue.setText("")

        # MP 110517: Convert from Dialog to DockWidget
        self.dlg1.setWindowTitle('PhotoInterpreter %s' % self.version)
        # Show and add DockWidget in LeftDockWidgetArea
        self.iface.addDockWidget(Qt.RightDockWidgetArea, self.dlg1)

        # Disable gui controls
        self.enable_controls(False)

    def enable_controls(self, flag=True):
        # self.dlg1.boxAttributeControl.setEnabled(flag)
        self.dlg1.boxMainControls.setEnabled(flag)
        self.dlg1.boxSearch.setEnabled(flag)

    def enable_ranks(self, flag):
        self.dlg1.btnRank1.setEnabled(flag)
        self.dlg1.btnRank2.setEnabled(flag)
        self.dlg1.btnRank3.setEnabled(flag)
        self.dlg1.btnRank4.setEnabled(flag)

    def close_plugin(self):
        # Disconect listeners
        self.disconnect_listeners()
        self.layer = None
        self.f_name = None
        self.com_col = None
        self.field_idx = None
        self.fid_to_idx = None
        self.idx_to_fid = None
        self.feat_id_lst = None
        self.search_lst_pos = None
        self.flag_idx = None
        self.values = []

    def layersRemoved(self, lyr_list):
        # If no loaded layer, do nothing
        if not self.layer:
            return
        for lyr_id in lyr_list:
            if self.layer and lyr_id == self.layer.id():
                print("removing main layer!", self.layer.id())
                self.layer = None
                self.f_name = None
                self.com_col = None
                self.field_idx = None
                self.fid_to_idx = None
                self.idx_to_fid = None
                self.feat_id_lst = None
                self.search_lst_pos = None
                self.flag_idx = None
                self.values = []
                # Clear ui and drop references to current main layer
                self.update_info_ui(None)
                self.dlg1.txtCurrentLayerName.setText("")
                self.dlg1.lblRequestCounter.setText("")
                self.clean_search_ui()
                # Disable ui
                self.enable_controls(False)

    def loadValuesDef(self):
        self.loadValues(ask_path=True)
        
    def addCol(self):
        self.dlg4.addColNameLineEdit.setText("II_")
        self.dlg4.show()
    
    def addColCancel(self):
        self.dlg4.addColNameLineEdit.clear()
        self.dlg4.hide()
        
    def addColOk(self):
        newColName = self.dlg4.addColNameLineEdit.text()
        if self.layer.fields().indexFromName(newColName) < 0:
            self.layer.dataProvider().addAttributes([QgsField(newColName, QVariant.String, len=150)])
            self.layer.updateFields()
        if self.layer.fields().indexFromName(newColName) >= 0 \
                and self.layer.fields().field(newColName).type() != QVariant.String:
            idx = self.layer.fields().indexFromName(newColName)
            print("column has not a string type: renaming in old_II_default")
            self.layer.dataProvider().renameAttributes({idx: f"old_{newColName}"})
            self.layer.updateFields()

        self.dlg1.firstColCombo.addItem(newColName)
        self.layer.updateFields()
        self.dlg4.hide()

    def enterValues(self):
        # Show the window to enter values
        self.dlg3.show()

    def enterValuesOk(self):
        self.value1 = self.dlg3.txtValue1.text()
        self.value2 = self.dlg3.txtValue2.text()
        self.value3 = self.dlg3.txtValue3.text()
        self.value4 = self.dlg3.txtValue4.text()
        self.value5 = self.dlg3.txtValue5.text()
        self.value6 = self.dlg3.txtValue6.text()

        val_list = [self.value1, self.value2, self.value3, self.value4, self.value5, self.value6]
        ranks = [self.value1, self.value2, self.value3, self.value4]
        self.val_list = val_list
        self.symboName()

        rank_dict = {}
        for rank in rank_dict:
            if rank < 5:
                ranks[rank - 1] = rank_dict[rank]
        self.values = self.val_list
        self.ranks = ranks

        # Clear comboboxes
        self.dlg1.cbbValues.clear()
        self.dlg1.cbbFindValues.clear()
        self.dlg1.cbbDefaultValues.clear()
        # Add new values
        for val in self.values:
            self.dlg1.cbbValues.addItem(val)
            self.dlg1.cbbFindValues.addItem(val)
            self.dlg1.cbbDefaultValues.addItem(val)

        # Disable all buttons, then enable needed number of buttons
        self.enable_ranks(False)
        # Update ranks on buttons
        x = 0
        for l in range(0, len(self.ranks)):
            if (self.ranks[l] != ""):
                x = x +1

        for i in range(0, x):
            btn = getattr(self.dlg1, "btnRank%s" % (i + 1))
            # Truncate values if too large
            btn.setText("%s >>" % self.ranks[i][:7])
            btn.setEnabled(True)
        self.dlg3.hide()


    def enterValuesCancel(self):
        self.dlg3.hide()

    def createLayer(self):
        self.dlg2.show()
        pjt = QgsProject.instance()
        for layer in pjt.mapLayers():
            self.dlg2.cbbEmprise.addItem(layer)

    def createLayerTest(self):

        if self.dlg2.cbbEmprise.currentText() != "None":
            self.emprise = self.dlg2.cbbEmprise.currentText()

        self.disttest = self.dlg2.distLineEdit.text()
        try:
            self.disttest = float(self.disttest)
        except ValueError:
            QMessageBox.critical(self.dlg2, "Error", "Error: please enter a numeric value.")
        else:
            try:
                self.dist = self.disttest

                pjt = QgsProject.instance()
                emprise_layer = pjt.mapLayer(self.emprise)
                if not self.is_valid_layer(emprise_layer):
                    QMessageBox.critical(self.dlg2, "Error", "Error: please select a vector layer!")
                    return
                emprise_ext = pjt.mapLayer(self.emprise).extent()
                self.extt = str(emprise_ext.xMinimum()) + ',' + str(emprise_ext.xMaximum()) + ',' + str(
                    emprise_ext.yMinimum()) + ',' + str(emprise_ext.yMaximum())

                result1 = processing.run("native:creategrid",
                                         {'CRS': emprise_layer.crs(),
                                          'EXTENT': self.extt,
                                          'HOVERLAY': 0, 'HSPACING': self.dist, 'OUTPUT': 'TEMPORARY_OUTPUT', 'TYPE': 0,
                                          'VOVERLAY': 0,
                                          'VSPACING': self.dist})
                grid_layer_transi = result1['OUTPUT']
                QgsProject.instance().addMapLayer(grid_layer_transi)
                grid_layer_transi.setName('grid_transi')

                processing.run("native:selectbylocation",
                                 {'INPUT': grid_layer_transi,
                                  'INTERSECT': emprise_layer,
                                  'METHOD': 0, 'PREDICATE': [6]})
                result3 = processing.run("native:saveselectedfeatures",
                                         {'INPUT': grid_layer_transi, 'OUTPUT': 'memory:'})
                grid_layer = result3['OUTPUT']
                QgsProject.instance().addMapLayer(grid_layer)
                grid_layer.setName('grid')
                QgsProject.instance().removeMapLayer(grid_layer_transi)
            except QgsProcessingException:
                QMessageBox.critical(self.dlg2, "Error", "There was a small problem with the values you entered. \n"
                                                           "\n"
                                                           "It is likely due to the fact that your Bounding layers default "
                                                           "length unit is degre and not meters (i.e. CRS 4326). \n"
                                                           "\n"
                                                           "You can check this information in your layers properties "
                                                           "(Properties > Informations > Unit). \n"
                                                           "\n"
                                                           "This problem can be solved either by converting your layer to "
                                                            "another CRS or by dividing the distance you entered by "
                                                           "approximately 100")

    def createLayerSave(self):
        patth = str(QFileDialog.getExistingDirectory())
        grid = iface.activeLayer()
        name = str(self.dlg2.nameLineEdit.text())

        processing.run("native:savefeatures",
                       {'DATASOURCE_OPTIONS': '', 'INPUT': grid, 'LAYER_NAME': '',
                        'LAYER_OPTIONS': '',
                        'OUTPUT': f"{patth}/{name}.shp"})
        #Hide grid layer
        node = QgsProject.instance().layerTreeRoot().findLayer(grid)
        node.setItemVisibilityChecked(False)
        #Save grid and choose path
        saved_grid = QgsVectorLayer("{}/{}.shp".format(patth, name), name, "ogr")
        QgsProject.instance().addMapLayer(saved_grid)
        self.dlg2.cbbEmprise.clear()
        self.dlg2.hide()
        self.loadVectorLayer()

    def createLayerClose(self):
        self.dlg2.cbbEmprise.clear()
        self.dlg2.hide()

    def commentValue(self):
        # Add comment column (com_col)
        if self.layer.fields().indexFromName("com_col") < 0:
            self.layer.dataProvider().addAttributes([QgsField("com_col", QVariant.String, len=150)])
            self.layer.updateFields()
        if self.layer.fields().indexFromName("com_col") >= 0 \
                and self.layer.fields().field("com_col").type() != QVariant.String:
            idx = self.layer.fields().indexFromName("com_col")
            print("column com_col has not a string type: renaming in old_com_col")
            self.layer.dataProvider().renameAttributes({idx: "old_com_col"})
            self.layer.updateFields()
        
        #writing comment in com_col
        comment = self.dlg1.commentLineEdit.text()
        for featu in self.layer.selectedFeatures():
            self.layer.startEditing()
            featu['com_col'] = str(comment)
            self.layer.updateFeature(featu)
            self.layer.commitChanges()
        self.updateField(None)

    def loadValues(self, ask_path):
        if ask_path:
            # Load list of values from csv file
            options = QFileDialog.Options()
            file_path, _ = QFileDialog.getOpenFileName(None, "Load values", "",
                                                       "All Files (*);;Csv Files (*.csv)", options=options)
            if len(file_path) == 0:
                print("empty file_path")
                return
        else:
            file_path = self.data_path.joinpath("values.csv").absolute()
        file_path = Path(file_path)

        if not (file_path.exists() and file_path.is_file()):
            print("wrong file_path", file_path.absolute())
            return

        print("loading file", file_path.absolute())
        val_list = []
        rank_dict = {}
        ranks = []
        # Open values file
        with open(file_path.absolute(), newline='') as csvfile:
            reader = csv.reader(csvfile, quoting=csv.QUOTE_ALL, skipinitialspace=True)
            header = None
            i = 0
            for row in reader:
                if header is None:
                    header = row
                else:
                    val_list.append(row[0])
                    # Get ranks
                    try:
                        rank = int(row[1])
                    except Exception as e:
                        print(e)
                        rank = i
                    # Append only first 4 ranks
                    if i < 5:
                        ranks.append(row[0])
                    rank_dict[rank] = row[0]
                i += 1
        # Check len val_list
        if len(val_list) == 0:
            return

        # List validated, copy items
        if ask_path:
            # Rename old values.csv in data folder
            os.rename(self.data_path.joinpath("values.csv").absolute(),
                      self.data_path.joinpath("values_%s.csv" % datetime.now().timestamp()).absolute())
            # Copy new file to data folder with name values.csv
            shutil.copy(file_path, self.data_path.absolute())
            # Rename new copied file
            os.rename(self.data_path.joinpath(file_path.name).absolute(),
                      self.data_path.joinpath("values.csv").absolute())
        # Prepare ranks
        for rank in rank_dict:
            if rank < 5:
                ranks[rank - 1] = rank_dict[rank]
        self.val_list = val_list
        self.values = self.val_list
        self.ranks = ranks

        # Clear comboboxes
        self.dlg1.cbbValues.clear()
        self.dlg1.cbbFindValues.clear()
        self.dlg1.cbbDefaultValues.clear()
        # Add new values
        for val in self.values:
            self.dlg1.cbbValues.addItem(val)
            self.dlg1.cbbFindValues.addItem(val)
            self.dlg1.cbbDefaultValues.addItem(val)

        # Disable all buttons, then enable needed number of buttons
        self.enable_ranks(False)
        # Update ranks on buttons
        for i in range(0, len(self.ranks)):
            btn = getattr(self.dlg1, "btnRank%s" % (i + 1))
            # Truncate values if too large
            btn.setText("%s >>" % self.ranks[i][:7])
            btn.setEnabled(True)
        self.updateSymbology()
        self.symboName()

    def symboName(self):
        # Load the name of the classes on the symbology
        try:
            root = QgsProject.instance().layerTreeRoot()
            node = root.findLayer(self.layer.id())
            y = 0
            try:
                while y < 11:
                    QgsMapLayerLegendUtils.setLegendNodeUserLabel(node, y, "")
                    y = y + 1

            except IndexError:
                pass
            x = 0
            try:
                while x < 11:
                    QgsMapLayerLegendUtils.setLegendNodeUserLabel(node, x, self.val_list[x])
                    x = x + 1
            except IndexError:
                pass
            iface.layerTreeView().refreshLayerSymbology(self.layer.id())
        except AttributeError:
            pass

    def loadVectorLayer(self):
        self.layer = iface.activeLayer()
        print("Loading selected layer...")
        # Check if we are working with a vector layer
        if not self.is_valid_layer(self.layer):
            QMessageBox.critical(self.dlg1, "Error", "Error: please select a vector layer!")
            return
        # Check is visible
        node = QgsProject.instance().layerTreeRoot().findLayer(self.layer)
        node.setItemVisibilityChecked(True)

        # Clear selection
        self.layer.removeSelection()
        self.dlg1.txtCurrentLayerName.setText(self.layer.name())

        # MP 151216: Begin editing
        self.layer.startEditing()
        
        # Add fields
        self.dlg1.firstColCombo.clear()
        for f in self.layer.dataProvider().fields():
            if f.type() == QVariant.String \
                    and f.name() != "com_col" \
                    and f.name() != "flag" \
                    and f.name().upper() != "ID" \
                    and f.name().upper().startswith("II_"):
                self.dlg1.firstColCombo.addItem(f.name())

        if self.dlg1.firstColCombo.count() == 0:
            # if column II_default exists but not of type string, rename it
            if self.layer.fields().indexFromName("II_default") >= 0 \
                    and self.layer.fields().field("II_default").type() != QVariant.String:
                idx = self.layer.fields().indexFromName("II_default")
                print("column II_default has not a string type: renaming in old_II_default")
                self.layer.dataProvider().renameAttributes({idx: "old_II_default"})
                self.layer.updateFields()

            # MP 151216: # Add one default field: II_default
            if self.layer.fields().indexFromName("II_default") < 0:
                self.layer.dataProvider().addAttributes([QgsField("II_default", QVariant.String, len=254)])
                self.layer.updateFields()
                # add new attribute to combobox
                self.dlg1.firstColCombo.addItem("II_default")
                self.dlg1.firstColCombo.setCurrentIndex(0)
        else:
            self.dlg1.firstColCombo.setCurrentIndex(0)

        # Find or add a flag field
        self.flag_idx = self.layer.fields().indexFromName("flag")
        if self.flag_idx < 0:
            self.layer.dataProvider().addAttributes([QgsField("flag", QVariant.Int, len=1)])
            self.layer.updateFields()
            self.flag_idx = self.layer.fields().indexFromName("flag")
        elif self.layer.dataProvider().fields()[self.flag_idx].type() != QVariant.Int:
            QMessageBox.critical(self.dlg1, "Error", "There already exists a 'flag' field but with a wrong type")
            return

        if self.layer.fields().indexFromName("id_order") < 0:
            self.layer.dataProvider().addAttributes([QgsField("id_order", QVariant.LongLong, len=10)])
            self.layer.updateFields()

            # Update attribute with feature id
            for feature in self.layer.getFeatures():
                feature['id_order'] = feature.id()
                # Update feature
                self.layer.updateFeature(feature)

        if self.layer.fields().indexFromName("num_value") < 0:
            self.layer.dataProvider().addAttributes([QgsField("num_value", QVariant.LongLong, len=10)])
            self.layer.updateFields()

        # MP 151216: Call commit to save the changes
        self.layer.commitChanges()

        self.init_fid_to_idx()
        self.select_default()
        self.enable_controls(True)
        self.updateSymbology()
        self.symboName()

    def is_valid_layer(self, layer):
        try:
            if layer.type() != QgsMapLayer.VectorLayer: return False
            return True
        except:
            return False

    def fieldChanged(self):
        self.f_name = self.dlg1.firstColCombo.currentText()
        self.field_idx = self.layer.fields().indexFromName(self.f_name)

        if self.field_idx < 0:
            return
        # Set current value to nothing
        # self.layer.removeSelection()
        self.dlg1.txtCurrentValue.setText("")

    def init_fid_to_idx(self):
        if self.layer:
            idx = 1
            self.fid_to_idx, self.idx_to_fid = {}, {}
            for f in self.layer.getFeatures():
                self.fid_to_idx[f.id()] = idx
                self.idx_to_fid[idx] = f.id()
                idx += 1

    def updateField(self, value):
        try:
            if value is None:
                if self.dlg1.cbbValues.currentText() != "None":
                    value = self.dlg1.cbbValues.currentText()

            for feat in self.layer.selectedFeatures():
                # MP 151216: Begin editing
                self.layer.startEditing()
                # Update attribute
                feat[self.f_name] = value
                feat[self.flag_idx] = int(self.dlg1.cbxFlag.isChecked())
                # Update feature
                self.layer.updateFeature(feat)
                # Call commit to save the changes
                self.layer.commitChanges()

            self.dlg1.txtCurrentValue.setText(str(value))
            self.dlg1.cbbValues.setCurrentIndex(self.dlg1.cbbDefaultValues.currentIndex())

            for featu in self.layer.selectedFeatures():
                self.layer.startEditing()
                featu['num_value'] = self.values.index(featu[self.f_name])
                self.layer.updateFeature(featu)
                self.layer.commitChanges()

        except Exception as e:
            print((traceback.format_exc()))
            QMessageBox.critical(self.dlg1, "Error", str(e))

    def updateFieldNext(self):
        self.updateField(None)
        self.nextID()

    def rankUpdateNext(self, rank):
        self.updateField(self.ranks[rank])
        self.nextID()

    def select_feature(self, offset, zoom=False):
        try:
            selected_feats = self.layer.selectedFeatures()
            if len(selected_feats) < 1:
                # Nothing selected, select default feature
                self.select_default()
                return

            fid = selected_feats[0].id()
            idx = self.fid_to_idx[fid] + offset
            if idx > len(self.idx_to_fid):
                idx = 1
            elif idx < 1:
                idx = len(self.idx_to_fid)
            fid = self.idx_to_fid[idx]
            self.layer.selectByIds([fid])

            selected_feats = self.layer.selectedFeatures()
            if len(selected_feats) > 0:
                cur_feat = selected_feats[0]
                # Update general template fields
                self.update_info_ui(cur_feat)

            if zoom:
                self.canvas.zoomToSelected()
        except Exception:
            print((traceback.format_exc()))
            self.update_info_ui(None)

    def select_default(self):
        try:
            selected_feats = self.layer.selectedFeatures()
            # If no selected feature, select first
            if len(selected_feats) < 1:
                self.layer.select(0)
                first_feat = self.layer.selectedFeatures()[0]
                # Activate layer and pan to selected
                self.activate_main_layer()
                # Show template feature
                self.update_info_ui(first_feat)
        except Exception:
            print((traceback.format_exc()))

    def previousID(self):
        self.select_feature(-1)
        self.activate_main_layer()

    def nextID(self):
        self.select_feature(1)
        self.activate_main_layer()

    def updateSymbology(self):
        try:
            if self.layer.wkbType() == QgsWkbTypes.Point:
                self.layer.loadNamedStyle(f"{self.plugin_dir}\symbo.qml")
            self.layer.triggerRepaint()
        except AttributeError:
            print("Symbology not loaded")

    def setSelectTool(self):
        expre1 = "{} = '{}'".format(self.f_name, self.dlg1.txtCurrentValue.text())
        processing.run("qgis:selectbyexpression",
                         {'INPUT': self.layer, 'EXPRESSION': expre1, 'OUTPUT': 'memory:'})

    def searchValue(self):
        # Search by value
        try:
            self.feat_id_lst = []
            # WHERE clause
            req = "%s IS NULL" % self.f_name
            if self.dlg1.cbbFindValues.currentText() != "None":
                req = "%s = '%s'" % (self.f_name, self.dlg1.cbbFindValues.currentText())
            # Search for req
            self.search_expr(req)
        except Exception as e:
            QMessageBox.critical(self.dlg1, "Error", str(e))

    def searchExpression(self):
        req = str(self.dlg1.teeRequest.toPlainText().strip())
        self.search_expr(req)

    def search_expr(self, req):
        # Search by expression
        try:
            self.layer.removeSelection()
            # Clean ui
            self.clean_search_ui()

            if len(req) == 0:
                raise Exception("The WHERE clause is empty")

            request = QgsFeatureRequest().setFilterExpression(req)
            self.feat_id_lst = [f.id() for f in self.layer.getFeatures(request)]

            if len(self.feat_id_lst) == 0:
                self.layer.removeSelection()
                self.update_info_ui()
                raise Exception("Nothing was found")
            else:
                self.layer.selectByIds(self.feat_id_lst)
                self.dlg1.lblRequestCounter.setText("0 / %s" % (len(self.feat_id_lst)))
                # Set next selected feature
                self.requestNext()
        except Exception as e:
            QMessageBox.critical(self.dlg1, "Error", str(e))

    def clean_search_ui(self):
        self.feat_id_lst = []
        self.search_lst_pos = -1
        self.dlg1.txtSearchCurrentValue.setText("")
        self.dlg1.lblRequestCounter.setText("")

    def update_info_ui(self, feat=None):
        # Update general template fields
        val_field = ""
        val_order = ""
        val_flag = False
        if feat:
            val_field = str(feat.attribute(self.field_idx))
            # get id_order attribute value
            val_order = str(feat.attribute("id_order"))
            # get the flag value
            if feat.attribute("flag") == 1:
                val_flag = True
        self.dlg1.txtCurrentValue.setText(val_field)
        self.dlg1.lblOrder.setText(val_order)
        self.dlg1.lblOrder.setText(val_order)
        self.dlg1.cbxFlag.setChecked(val_flag)

    def requestNext(self):
        self.go_to_selected(1)
        self.activate_main_layer()

    def requestPrevious(self):
        self.go_to_selected(-1)
        self.activate_main_layer()

    def go_to_selected(self, offset):
        try:
            if len(self.feat_id_lst) == 0:
                raise Exception("A valid search hasn't been done yet")

            self.search_lst_pos += offset
            if self.search_lst_pos < 0:
                self.search_lst_pos = len(self.feat_id_lst) - 1
            elif self.search_lst_pos > len(self.feat_id_lst) - 1:
                self.search_lst_pos = 0
            
            sel_feat = self.layer.getFeature(self.feat_id_lst[self.search_lst_pos])
            self.layer.selectByIds([sel_feat.id()])
            self.canvas.zoomToSelected()
            self.dlg1.txtSearchCurrentValue.setText(str(sel_feat[self.field_idx]))
            self.dlg1.lblRequestCounter.setText("%s / %s" % (self.search_lst_pos + 1, len(self.feat_id_lst)))
                       
            # Update general template fields
            self.update_info_ui(sel_feat)

            # Self.activate_main_layer()
        except Exception as e:
            QMessageBox.critical(self.dlg1, "Error", str(e))

    def activate_main_layer(self):
        # Select base layer
        self.iface.setActiveLayer(self.layer)
        # Pan to selection
        self.canvas.panToSelected()

    def showInfoSearch(self):
        # Show info search
        info = InfoDialog()
        info.exec_()

    def resolve(self, name, basepath=None):
        if not basepath:
            basepath = os.path.dirname(os.path.realpath(__file__))
        return os.path.join(basepath, name)

    def showInfoUsage(self):
        # Show info search
        info = UsageDialog(self)
        info.exec_()
