# coding: utf-8

from PyQt5.QtWidgets import QDialog, QDialogButtonBox, QApplication, QTextBrowser, QDesktopWidget, QVBoxLayout, \
    QPushButton
import os.path
from qgis.PyQt import uic

uiLoadCreateLayerDialog = uic.loadUiType(os.path.join(os.path.dirname(__file__), 'ui_create_layer.ui'))[0]


class CreateLayerDialog(QDialog, uiLoadCreateLayerDialog):

    def __init__(self):
        QDialog.__init__(self)
        (CreateLayerDialog, self).__init__()
        self.setupUi(self)
