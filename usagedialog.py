# coding: utf-8

from PyQt5.QtWidgets import QDialog, QDialogButtonBox, QApplication, QDesktopWidget, QVBoxLayout
from PyQt5.QtCore import Qt, QUrl
from PyQt5.QtWebKitWidgets import QWebView
from qgis.core import *
from qgis.gui import *
import os.path
import sys
import jinja2


class UsageDialog(QDialog):

    def __init__(self, pint):
        QDialog.__init__(self)

        self.pint = pint
        self.setFixedSize(550, 340)
        self.setWindowTitle("Usage information")

        fg = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        fg.moveCenter(cp)
        self.move(fg.topLeft())

        self.vertLayout = QVBoxLayout(self)
        self.vertLayout.setSpacing(5)
        self.vertLayout.setAlignment(Qt.AlignTop)
        self.setLayout(self.vertLayout)

        self.webView = QWebView(self)
        self.webView.setUrl(QUrl("about:blank"))
        self.webView.setObjectName("webView")

        self.buttonBox = QDialogButtonBox(QDialogButtonBox.Ok)
        self.buttonBox.accepted.connect(self.close_dialog)

        # Add widgets to layout
        self.vertLayout.addWidget(self.webView)
        self.vertLayout.addWidget(self.buttonBox)

        # values to show
        html = self.collect_usage()
        self.webView.setHtml(html)

    def close_dialog(self):
        self.close()

    def collect_usage(self):
        # from current layer
        layer = self.pint.layer
        values = {i: [0, True] for i in self.pint.values}
        # remove default None value
        #values.pop("None")
        ptTot, ptDone = layer.dataProvider().featureCount(), 0
        attr_name = str(self.pint.dlg1.firstColCombo.currentText())
        # update ptDone
        for f in layer.getFeatures():
            # no QPyNullVariant in pyqt5, converted to None ( and not isinstance(f[self.f_idx], QPyNullVariant))
            # https://www.riverbankcomputing.com/static/Docs/PyQt5/pyqt_qvariant.html
            # if no value, get NULL PyQt5.QtCore.QVariant from qgis3, to solve use check value != NULL
            val = f[self.pint.field_idx]
            if val != "" and val is not None and val != NULL:
                ptDone += 1
                if val in values.keys():
                    values[val][0] += 1
                else:
                    # value not in current values list
                    values[val] = [1, False]

        data = {"values": values, "done": ptDone, "tot": ptTot, "attr": attr_name}
        html = self.get_html_content(data)
        return html

    def get_html_content(self, data):
        self.plugin_dir = os.path.dirname(__file__)
        env = jinja2.Environment(loader=jinja2.FileSystemLoader(f"{self.plugin_dir}/template"))
        template = env.get_template('usage_stat.html')

        # Prepare data for Jinja template
        table_rows = []
        ext_count = 0
        for val, info in data["values"].items():
            ext_val = ""
            if not info[1]:
                ext_val = True
            table_rows.append({'val': val,
                               'nbr': info[0],
                               'pct': round(info[0]/data.get("tot"), 3),
                               'ext': ext_val})
            if info[1] is False:
                ext_count += 1
        output_html = template.render(table_rows=table_rows, done=data["done"], tot=data["tot"],
                                      ext_count=ext_count, attr=data["attr"])
        return output_html

    if __name__ == '__main__':
        app = QApplication(sys.argv)

        dialog = UsageDialog(None)
        dialog.show()

        sys.exit(app.exec_())
