# coding: utf-8

from PyQt5.QtWidgets import QDialog, QDialogButtonBox, QApplication, QTextBrowser, QDesktopWidget, QVBoxLayout, \
    QPushButton
import os.path
from qgis.PyQt import uic

uiLoadEnterValuesDialog = uic.loadUiType(os.path.join(os.path.dirname(__file__), 'ui_enter_values.ui'))[0]

class EnterValuesDialog(QDialog, uiLoadEnterValuesDialog):

    def __init__(self):
        QDialog.__init__(self)
        (EnterValuesDialog, self).__init__()
        self.setupUi(self)

