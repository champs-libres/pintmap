# -*- coding: utf-8 -*-
"""
/***************************************************************************
 PintMap
                                 A QGIS plugin
 Photo interpretation
                             -------------------
        begin                : 2014-07-02
        copyright            : (C) 2014 by GbxABT
        email                : nope@nope.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""

def classFactory(iface):
    # load PintMap class from file PintMap
    from .pint import Pint
    return Pint(iface)
