# -*- coding: utf-8 -*-
# coding: utf-8

from PyQt5.QtWidgets import QDialog, QDialogButtonBox, QApplication, QTextBrowser, QDesktopWidget, QVBoxLayout, \
    QPushButton
import os.path
from qgis.PyQt import uic

uiLoadAddColDialog = uic.loadUiType(os.path.join(os.path.dirname(__file__), 'ui_add_col.ui'))[0]

class AddColDialog(QDialog, uiLoadAddColDialog):

    def __init__(self):
        QDialog.__init__(self)
        (AddColDialog, self).__init__()
        self.setupUi(self)

